Summary

(Summarize the problem / Describe the business objectives)

Requirement Solving

(Design a solution through use cases, UX/UI,... / Learn knowledge)

Architecture Solving

(Design an architecture for the solution)

Implementation

(Create unit tests / Implement the solution)

Unit testing

(Run the unit tests)

Interaction testing

(Design interaction tests / Run the tests)

System testing

(Design system tests / Run the tests)